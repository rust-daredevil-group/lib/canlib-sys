# canlib-sys

This crate provides Rust bindings for Kvaser's CANlib.
Not all features/functions are implemented yet and might
not work. Use this at your own risk. 

## Prerequisites

First install header files and compiler for kernel compilation
(needed to build Kvaser's drivers).  

In Ubuntu: 

```
$ sudo apt-get install build-essential
```

In Fedora:  

```
$ sudo dnf install kernel-devel kernel-headers
```

You will need to install the Kvaser drivers as well as the CANlib
SDK. You can download the Linux drivers from:  
https://www.kvaser.com/downloads-kvaser/

Then do the following to build and install the drivers
```
$ tar xvzf linuxcan.tar.gz  
$ cd linuxcan  
$ make  
$ sudo make install
```
If you're running Secure Boot on your Linux computer you might want to
read this guide instead:  
https://www.kvaser.com/developer-blog/build-install-signed-kvaser-driver-modules/

## Installation

Add the following to your dependencies:

```
// Link to repository
[dependencies]
canlib-sys = { git = "https://gitlab.com/rust-daredevil-group/canlib-sys.git" }
```
Since this is a private repository you might need to login with ssh. 
```
// Link to repository
[dependencies]
canlib-sys = { git = "ssh://git@gitlab.com/rust-daredevil-group/canlib-sys.git" }
```
Otherwise you can just download it and add
```
// Link to local
[dependencies]
canlib-sys = { path = "/path/to/canlib-sys" }
```

## Usage

Check the examples for an idea on how to use this crate. For documentation
on the functions and other things, please refer to the official CANlib 
documentation.
  
A few things worth noting,

1) Use `*mut variable` in parameters where a reference is needed.
2) Add the crate `libc` to get access to 'C-like' type definitions.